from rest_framework import serializers

from movies.models import Movie, Review


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = ['id', 'author', 'text', 'likes', 'movie']


class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = ['id', 'name', 'release_year', 'poster_url']